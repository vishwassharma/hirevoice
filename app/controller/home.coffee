Controller = require 'lib/controller/controller'
HomePageView = require 'views/home_view'

class HomePageController extends Controller
  
  render : (params) ->
    @view = new HomePageView
    console.log params

module.exports = HomePageController