Controller = require 'lib/controller/controller'
BreadCrum = require 'models/breadcrum'
BreadCrumView = require 'views/breadcrum_view'

class BreadCrumController extends Controller
  
  initialize : ->
    super
    @breadcrum = new BreadCrum
    @view = new BreadCrumView model: @breadcrum


module.exports = BreadCrumController
