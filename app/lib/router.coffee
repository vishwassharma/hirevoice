#application = require 'application'
mediator = require 'lib/mediator'
ProviderHomeController = require 'controller/provider_home'
ResumeController = require 'controller/resume'
JobsController = require 'controller/jobs'
HomePageController = require 'controller/home'
SeekerController = require 'controller/seeker'

module.exports = class Router extends Backbone.Router

  initialize : (options) ->
    super
    @homepage = new HomePageController
    @provider = new ProviderHomeController
    @resumePage = new ResumeController
    @jobPage = new JobsController
    @seekerPage = new SeekerController

  routes:
    '' : 'home'
    'seeker/:id' : 'seeker_home'
    'provider/:id' : 'provider_home'
    'jobs/:id' : 'jobs'
    'resume/:id' : 'resume'
    'callback' : 'callback'


  callback : (options) ->
    console.log options
    mediator.publish 'login', @
    mediator.publish 'loginSuccess', @
    console.log window.opener.application
    #window.close()

  home : ->
    # controller = new HomePageController
    options =
      a : 10
    @homepage.render options

  provider_home: (id) ->
    # controller = new ProviderHomeController
    options =
      id : id
    @provider.render options

  resume : (id) ->
    # resumePage = new ResumeController
    options =
      id : id
    @resumePage.home options
    #$('#magic').html application.resumeView.render().el

  jobs: (id) ->
    # jobPage = new JobsController
    options =
      id : id
    @jobPage.home options
    # view = application.jobsView
    #$('#magic').html application.jobsView.render().el
    #$('#container').html application.jobsView.render().el

  seeker : (id) ->
    console.log "[Router] Seeker #{id}"
    # seekerPage = new SeekerController
    options =
      id : id
    @seekerPage.home options