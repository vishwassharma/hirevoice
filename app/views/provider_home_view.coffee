View = require 'lib/views/view'
CollectionView = require 'lib/views/collection_view'
mediator = require 'lib/mediator'
# --------------------------------------
template = require './templates/home'
headerTemplate  = require './templates/home/header'
newJobTemplate = require './templates/home/newjob'
optionsTemplate = require './templates/home/options'
jobsTemplate = require './templates/home/jobs'
home = require 'models/home'
jobTemplate = require './templates/home/job'


class JobView extends View
  template : jobTemplate
  tagName : 'tr'
  isChecked : false

  initialize : (options) ->
    super
    @subscribeEvent 'optionTrigger', @optionTrigger
    @delegate 'click', 'input:checkbox', @eventChecked
    @subscribeEvent 'optSelectAll', @evtSelectAll

  evtSelectAll : (data) =>
    @toggleCheckbox(data)

  toggleCheckbox : (state) ->
    @$('input:checkbox').prop("checked", state)

  eventChecked : (events) =>
    target  = events.target
    @isChecked = @checkState()
    @toggleCheckbox(@isChecked)

  checkState : =>
    target = @$('input:checked')
    return target.is(':checked')

  optionTrigger : (options) ->
    console.log options
    return unless @checkState()
    console.log @

  afterRender : =>
    super
    @$('a[rel=tooltip]')
      .tooltip()
      .click (e)->
        e.preventDefault()

        
collection = new home.Collection
class JobsView extends CollectionView
  template : jobsTemplate
  collection : collection
  tagName : 'tbody'
  # Append the item views to this element
  listSelector: 'tbody'
  itemView : JobView

  initialize: ->
    super
    @subscribeEvent 'newJobAdded' , @addModelView
    @collection.fetch()
  
  # The most important method a class derived from CollectionView
  # must overwrite.
  #getView: (item) ->
    # Instantiate an item view
    #new JobView model: item
  addModelView : (options) =>
    today = new Date()
    result =
      id : 23
      name : 'test'
      description : 'tsejldfajaljf lajfj lk fsdjklsdf ajkl'
      meta :
        date :
          day : today.getDate()
          month : today.getMonth()+1
          year : today.getFullYear()

    @collection.create result

class OptionsView extends View
  id : 'options'
  template : optionsTemplate
  autoRender : true

  initialize : (options) ->
    super
    @delegate 'click', '#selectAll', @eventSelectAll
    @delegate 'click', '#setting', @eventButtonSetting
    @delegate 'click', '#delete', @eventButtonDelete

  eventSelectAll : (event) =>
    result = false
    target  = event.target
    if $(target).is(':checked')
      result = true
    mediator.publish 'optSelectAll', result

  eventButtonSetting : (events) =>
    @pushOptions(events)

  eventButtonDelete : (events) =>
    @pushOptions(events)

  pushOptions : (events) =>
    mediator.publish 'optionTrigger', $(events.target)


class NewJobView extends View
  id : 'new-job'
  template : newJobTemplate
  className : 'well'
  autoRender: true

  initialize : (options) ->
    super
    @delegate 'click', 'button#addbutton', @getForm
    @delegate 'submit', '#job-form', @onSubmit

  getForm : (e) ->
    target = e.target
    $(target).toggleClass('hide').siblings().toggleClass('hide')

  toggleView : (target) ->
    $(target).parent().toggleClass('hide').siblings().toggleClass('hide')

  onSubmit : (e) =>
    # on submit close this view and add a new entry into the model
    target = e.target
    mediator.publish 'newJobAdded', $(target)
    @toggleView(target)
    false


class HeaderView extends View
  id : 'header'
  template : headerTemplate
  className : 'page-header'
  autoRender: true

class HomeView extends View
  id: 'home-view'
  template: template
  className : 'span12'
  hasSubView : true
  container : '#magic'
  autoRender : true

  #render : ->
    #console.log "before"
    # create some subviews
    # 1st view will contain header
    #super

  afterRender : ->
    super
    # adding subview
    # add header
    @headerView = @addSubView 'header', HeaderView, @$('#homeheader')
    # add new job button
    @newJobView = @addSubView 'newJob', NewJobView, @$('#new-job')
    # add options
    @optionView = @addSubView 'options', OptionsView, @$('#optionbox')
    # add table view
    @tableView  = @addSubView 'jobs', JobsView, @$('#jobsTable')
    @

module.exports = HomeView
