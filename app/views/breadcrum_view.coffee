View = require 'lib/views/view'
template = require './templates/breadcrum'

class BreadCrumView extends View
  id : "breadcrum-view"
  template: template
  #className : 'span12'
  container : '#breadcrum'
  autoRender: true

module.exports = BreadCrumView
