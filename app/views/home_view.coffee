View = require "lib/views/view"
template = require 'views/templates/home_page'

class HomePageView extends View
  id : "home-view"
  template: template
  className : 'span12'
  container : '#magic'
  hasSubView : true
  autoRender : true

  initialize: (options) ->
    super
    @twiiterbutton = $('button#twitterlogin')
    @delegate 'click', 'button#twitterlogin', @addLoading
    @subscribeEvent '!loginFail' , @addAuthFail
    @subscribeEvent 'loginSuccess', @addAuthSuccess

  addLoading : (event) =>
    target = event.target
    $(target).html('Authenticating ....')
  
  addAuthFail : (options) =>
    console.log options

  addAuthSuccess : (options) =>
    console.log options
    @twitterbutton.html 'Welcome Vishwas'



module.exports = HomePageView

