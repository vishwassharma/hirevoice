View = require 'lib/views/view'
template = require 'views/templates/resume'
resumeTemplate = require 'views/templates/resume/main1'
pageTemplate = require 'views/templates/resume/page'
pagesTemplate = require 'views/templates/resume/pages'
annotationsTemplate = require 'views/templates/resume/annotations'
detailTemplate = require 'views/templates/resume/detail'
rightTemplate = require 'views/templates/resume/right'
PageModel = require 'models/page'

class AnnotationDetail extends View
  template : detailTemplate
  autoRender : true

class AnnotationsView extends View
  id : 'annotations'
  basictemplate: annotationsTemplate
  autoRender : true
  hasSubView: true
  toggleTemplate : detailTemplate
  isDetailed : false

  # write anything to filter the data that has to be passed to template
  filter : (data) ->
    result = _.filter data['annotations'], (annotation) =>
                if annotation['page'] is @pagenumber
                  return annotation

    #result
    "annotations" : result


  initialize : ->
    @template = @basictemplate
    super

  events :
    'click .annotationWrapper' : 'clicked'

  clicked : (events) =>
    if @isDetailed
      @template = @basictemplate
      @isDetailed = false
    else
      @template = @toggleTemplate
      @isDetailed = true
    @render()
    events.preventDefault()

  normalize : =>
    @template = @basictemplate
    @render()


class PageView extends View
  #id : 'page'
  template : pageTemplate
  hasSubView : true
  containerMethod : 'append'

  initialize : (options) ->
    super
    @render {@pagenumber}


  render : (attr) ->
    super
    #_.extend attr, {@model}
    param = _.extend {}, attr, {@model}
    if @hasAnnotations
      @annotationsView = @addSubView 'annotations', AnnotationsView, @$('#annotations'), param
    @


class PagesView extends View
  id : 'pagesbig'
  template : pagesTemplate
  autoRender : true
  hasSubView : true

  render : ->
    super
    pages = @model.get('pages')
    context = {@model}
    # get the list of pages which contains annotations
    annotations = @model.get('annotations')
    pagesWithAnno = _.pluck(annotations, 'page')

    for i in [1..pages]
      meta =
        pagenumber : i
      # check if this perticular par has annotations or not
      if i in pagesWithAnno
        meta['hasAnnotations'] = true
      else
        meta['hasAnnotations'] = false
      param = _.extend {}, context, meta
      view = @addSubView 'page' , PageView , @$('#pagesbig'), param
    @



class RightView extends View
  template : rightTemplate
  autoRender: true


class ResumeView extends View
  id : 'pages'
  template : resumeTemplate
  #autoRender: true
  hasSubView : true

  initialize : ->
    @model = new PageModel  id : 21
    @modelBind 'change', @changeModel
    @model.fetch()
    super

  changeModel : (options) =>
    @renderSubViews model : options
    

  events:
    'click #pages' : 'clicked'

  renderSubViews : (options) =>
    @render()
    @pageView = @addSubView 'page' , PagesView, @$('#pages'), options
    @rightView = @addSubView 'rigth', RightView, @$('#rightdata'), options


  clicked : (events) =>
    @pageView.annotationsView.normalize()
    events.stopPropagation()

class ResumeMainView extends View
  id : "resume-view"
  template: template
  className : 'span12'
  hasSubView : true
  container : "#magic"
  autoRender : true

  afterRender: ->
    super

    # add some subviews
    @resumeView = @addSubView 'resume', ResumeView, @$('#resumeContainer')
    @

module.exports = ResumeMainView
