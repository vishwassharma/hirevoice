View = require "lib/views/view"
template = require 'views/templates/jobs'
JobModel = require 'models/jobs'
dataTemplate = require 'views/templates/jobs/data'

class DataView extends View
  id : "data-view"
  template: dataTemplate

  initialize : ->
    @model = new JobModel id : 21
    @modelBind 'change', @changeRender
    @model.fetch()

  changeRender: (options) ->
    #console.log options
    @render()


class JobsView extends View
  id : "jobs-view"
  template: template
  className : 'span12'
  container : '#magic'
  hasSubView : true
  autoRender : true

  afterRender: ->
    super

    # add some subviews
    @dataView = @addSubView 'data', DataView, @$('#data')
    @

module.exports = JobsView
