Model = require "lib/models/model"
Collection = require "lib/models/collection"

class Job extends Model
  # for each resume
  defaults :
    ntags : 0
    mails : 0
    resumes : 0
    applicant : 0
    resources :
      mail : '#mail/{{id}}/new'
      resume : '#resume/{{id}}/new'
      applicant : '#applicant/{{id}}/new'

  initialize : (options) ->
    super
    today = new Date()
    date:
      day : today.getDay()
      month : today.getMonth()
      year : today.getYear()


class JobList extends Collection
  url : 'data/home.json'
  model : Job
  
  initialize: ->
    super

#jobList = new JobList
#module.exports = jobList
exports.Collection = JobList
