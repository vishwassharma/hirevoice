(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return require(absolute);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    definition(module.exports, localRequire(name), module);
    var exports = cache[name] = module.exports;
    return exports;
  };

  var require = function(name) {
    var path = expand(name, '.');

    if (has(cache, path)) return cache[path];
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex];
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '"');
  };

  var define = function(bundle) {
    for (var key in bundle) {
      if (has(bundle, key)) {
        modules[key] = bundle[key];
      }
    }
  }

  globals.require = require;
  globals.require.define = define;
  globals.require.brunch = true;
})();

window.require.define({"application": function(exports, require, module) {
  var App, Application, Layout, mediator,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  mediator = require('lib/mediator');

  Layout = require('lib/views/layout');

  App = require('lib/application');

  Application = (function(_super) {

    __extends(Application, _super);

    function Application() {
      return Application.__super__.constructor.apply(this, arguments);
    }

    Application.prototype.title = 'HireVoice';

    Application.prototype.initialize = function(options) {
      console.log("[Application.coffee] Initialization function");
      Application.__super__.initialize.apply(this, arguments);
      this.initializeMediator();
      this.initializeRouter();
      this.initializeLayout();
      this.initializeController();
      return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
    };

    Application.prototype.initializeController = function() {
      return console.log("[Application.coffee] Initialization Controller");
    };

    Application.prototype.initializeMediator = function() {
      console.log("[Application.coffee] Initialization Mediator");
      return mediator.seal();
    };

    Application.prototype.initializeRouter = function() {
      var Router;
      console.log("[Application.coffee] Initialization Router");
      Router = require('lib/router');
      return this.router = new Router;
    };

    Application.prototype.initializeLayout = function() {
      var layout;
      console.log("[Application.coffee] Initialization Layout");
      return layout = new Layout({
        title: this.title
      });
    };

    return Application;

  })(App);

  module.exports = Application;
  
}});

window.require.define({"controller/breadcrum": function(exports, require, module) {
  var BreadCrum, BreadCrumController, BreadCrumView, Controller,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Controller = require('lib/controller/controller');

  BreadCrum = require('models/breadcrum');

  BreadCrumView = require('views/breadcrum_view');

  BreadCrumController = (function(_super) {

    __extends(BreadCrumController, _super);

    function BreadCrumController() {
      return BreadCrumController.__super__.constructor.apply(this, arguments);
    }

    BreadCrumController.prototype.initialize = function() {
      BreadCrumController.__super__.initialize.apply(this, arguments);
      this.breadcrum = new BreadCrum;
      return this.view = new BreadCrumView({
        model: this.breadcrum
      });
    };

    return BreadCrumController;

  })(Controller);

  module.exports = BreadCrumController;
  
}});

window.require.define({"controller/home": function(exports, require, module) {
  var Controller, HomePageController, HomePageView,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Controller = require('lib/controller/controller');

  HomePageView = require('views/home_view');

  HomePageController = (function(_super) {

    __extends(HomePageController, _super);

    function HomePageController() {
      return HomePageController.__super__.constructor.apply(this, arguments);
    }

    HomePageController.prototype.render = function(params) {
      this.view = new HomePageView;
      return console.log(params);
    };

    return HomePageController;

  })(Controller);

  module.exports = HomePageController;
  
}});

window.require.define({"controller/jobs": function(exports, require, module) {
  var Controller, JobsController, JobsView, Resume,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Controller = require('lib/controller/controller');

  Resume = require('models/resume');

  JobsView = require('views/jobs_view');

  JobsController = (function(_super) {

    __extends(JobsController, _super);

    function JobsController() {
      return JobsController.__super__.constructor.apply(this, arguments);
    }

    JobsController.prototype.home = function(params) {
      console.log(params);
      return this.view = new JobsView;
    };

    return JobsController;

  })(Controller);

  module.exports = JobsController;
  
}});

window.require.define({"controller/navigation": function(exports, require, module) {
  var Controller, Navigation,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Controller = require('lib/controller/controller');

  Navigation = (function(_super) {

    __extends(Navigation, _super);

    function Navigation() {
      return Navigation.__super__.constructor.apply(this, arguments);
    }

    return Navigation;

  })(Controller);

  module.exports = Navigation;
  
}});

window.require.define({"controller/provider_home": function(exports, require, module) {
  var Controller, Home, ProviderHomeController, ProviderHomeView,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Controller = require('lib/controller/controller');

  Home = require('models/home');

  ProviderHomeView = require('views/provider_home_view');

  ProviderHomeController = (function(_super) {

    __extends(ProviderHomeController, _super);

    function ProviderHomeController() {
      return ProviderHomeController.__super__.constructor.apply(this, arguments);
    }

    ProviderHomeController.prototype.render = function(parmas) {
      this.view = new ProviderHomeView;
      return console.log(parmas);
    };

    return ProviderHomeController;

  })(Controller);

  module.exports = ProviderHomeController;
  
}});

window.require.define({"controller/resume": function(exports, require, module) {
  var Controller, Resume, ResumeController, ResumeView,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Controller = require('lib/controller/controller');

  Resume = require('models/resume');

  ResumeView = require('views/resume_view');

  ResumeController = (function(_super) {

    __extends(ResumeController, _super);

    function ResumeController() {
      return ResumeController.__super__.constructor.apply(this, arguments);
    }

    ResumeController.prototype.home = function(params) {
      this.view = new ResumeView;
      return console.log(params);
    };

    return ResumeController;

  })(Controller);

  module.exports = ResumeController;
  
}});

window.require.define({"controller/seeker": function(exports, require, module) {
  var Controller, JobsController, JobsView, Resume,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Controller = require('lib/controller/controller');

  Resume = require('models/resume');

  JobsView = require('views/jobs_view');

  JobsController = (function(_super) {

    __extends(JobsController, _super);

    function JobsController() {
      return JobsController.__super__.constructor.apply(this, arguments);
    }

    JobsController.prototype.home = function(options) {
      console.log(options);
      return this.view = new JobsView;
    };

    return JobsController;

  })(Controller);

  module.exports = JobsController;
  
}});

window.require.define({"dispatcher": function(exports, require, module) {
  var Dispatcher, RouterDispatcher,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Dispatcher = require('lib/Dispatcher');

  RouterDispatcher = (function(_super) {

    __extends(RouterDispatcher, _super);

    function RouterDispatcher() {
      return RouterDispatcher.__super__.constructor.apply(this, arguments);
    }

    RouterDispatcher.prototype.initialize = function(options) {};

    return RouterDispatcher;

  })(Dispatcher);
  
}});

window.require.define({"initialize": function(exports, require, module) {
  
  $(function() {
    var Application, application;
    console.log("[Initialize.coffee] Application Initialization");
    Application = require('application');
    application = new Application;
    Backbone.history.start();
    $("a[rel=popover]").popover().click(function(e) {
      return e.preventDefault();
    });
    return $("a[rel=tooltip]").tooltip().click(function(e) {
      return e.preventDefault();
    });
  });
  
}});

window.require.define({"lib/application": function(exports, require, module) {
  var Application, Layout, mediator;

  mediator = require('lib/mediator');

  Layout = require('lib/views/layout');

  Application = (function() {

    Application.prototype.title = '';

    Application.prototype.layout = null;

    Application.prototype.router = null;

    function Application() {
      this.initialize.apply(this, arguments);
    }

    Application.prototype.initialize = function() {};

    Application.prototype.initializeMediator = function() {
      return mediator.seal();
    };

    Application.prototype.initializeRouter = function() {
      var Router;
      Router = require('lib/router');
      return this.router = new Router;
    };

    Application.prototype.initializeLayout = function(options) {
      var _ref;
      if (options == null) {
        options = {};
      }
      if ((_ref = options.title) == null) {
        options.title = this.title;
      }
      return this.layout = new Layout(options);
    };

    Application.prototype.disposed = false;

    Application.prototype.dispose = function() {
      var prop, properties, _i, _len;
      if (this.disposed) {
        return;
      }
      properties = ['dispatcher', 'layout', 'router'];
      for (_i = 0, _len = properties.length; _i < _len; _i++) {
        prop = properties[_i];
        this[prop].dispose();
        delete this[prop];
      }
      this.disposed = true;
      return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
    };

    return Application;

  })();

  module.exports = Application;
  
}});

window.require.define({"lib/controller/controller": function(exports, require, module) {
  var Controller, Subscriber, mediator,
    __hasProp = {}.hasOwnProperty;

  mediator = require('lib/mediator');

  Subscriber = require('lib/subscriber');

  Controller = (function() {

    _.extend(Controller.prototype, Subscriber);

    Controller.prototype.currentId = null;

    function Controller() {
      this.initialize.apply(this, arguments);
    }

    Controller.prototype.initialize = function(options) {};

    Controller.prototype.disposed = false;

    Controller.prototype.dispose = function() {
      var obj, prop, properties, _i, _len;
      if (this.disposed) {
        return;
      }
      for (prop in this) {
        if (!__hasProp.call(this, prop)) continue;
        obj = this[prop];
        if (obj && typeof obj.dispose === 'function') {
          obj.dispose();
          delete this[prop];
        }
      }
      this.unsubscribeAllEvents();
      properties = ['currentId'];
      for (_i = 0, _len = properties.length; _i < _len; _i++) {
        prop = properties[_i];
        delete this[prop];
      }
      this.disposed = true;
      return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
    };

    return Controller;

  })();

  module.exports = Controller;
  
}});

window.require.define({"lib/dispatcher": function(exports, require, module) {
  var Dispatcher, Subscriber;

  Subscriber = require('lib/subscriber');

  Dispatcher = (function() {

    _.extend(Dispatcher.prototype, Subscriber);

    function Dispatcher() {
      this.initialize.apply(this, arguments);
    }

    Dispatcher.prototype.initialize = function(options) {
      return this.subscriberEvent('matchRoute', this.matchRoute);
    };

    Dispatcher.prototype.matchRoute = function(options) {
      return console.log(options);
    };

    return Dispatcher;

  })();

  modular.exports = Dispatcher;
  
}});

window.require.define({"lib/mediator": function(exports, require, module) {
  var mediator, support, utils;

  support = require('lib/support');

  utils = require('./utils');

  mediator = {};

  mediator.subscribe = mediator.on = Backbone.Events.on;

  mediator.unsubscribe = mediator.off = Backbone.Events.off;

  mediator.publish = mediator.trigger = Backbone.Events.trigger;

  mediator._callbacks = null;

  utils.readonly(mediator, 'subscribe', 'unsubscribe', 'publish', 'on', 'off', 'trigger');

  mediator.seal = function() {
    if (support.propertyDescriptors && Object.seal) {
      return Object.seal(mediator);
    }
  };

  utils.readonly(mediator, 'seal');

  module.exports = mediator;
  
}});

window.require.define({"lib/models/collection": function(exports, require, module) {
  var Collection,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  module.exports = Collection = (function(_super) {

    __extends(Collection, _super);

    function Collection() {
      return Collection.__super__.constructor.apply(this, arguments);
    }

    return Collection;

  })(Backbone.Collection);
  
}});

window.require.define({"lib/models/model": function(exports, require, module) {
  var Model, Subscriber, utils,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  utils = require("lib/utils");

  Subscriber = require('lib/subscriber');

  Model = (function(_super) {
    var serializeAttributes;

    __extends(Model, _super);

    function Model() {
      return Model.__super__.constructor.apply(this, arguments);
    }

    _.extend(Model.prototype, Subscriber);

    Model.prototype.idAttribute = '_id';

    Model.prototype.getAttributes = function() {
      return this.attributes;
    };

    serializeAttributes = function(model, attributes, modelStack) {
      var delegator, item, key, value;
      if (!modelStack) {
        delegator = utils.beget(attributes);
        modelStack = [model];
      } else {
        modelStack.push(model);
      }
      for (key in attributes) {
        value = attributes[key];
        if (value instanceof Model) {
          if (delegator == null) {
            delegator = utils.beget(attributes);
          }
          delegator[key] = value === model || __indexOf.call(modelStack, value) >= 0 ? null : serializeAttributes(value, value.getAttributes(), modelStack);
        } else if (value instanceof Backbone.Collection) {
          if (delegator == null) {
            delegator = utils.beget(attributes);
          }
          delegator[key] = (function() {
            var _i, _len, _ref, _results;
            _ref = value.models;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              _results.push(serializeAttributes(item, item.getAttributes(), modelStack));
            }
            return _results;
          })();
        }
      }
      modelStack.pop();
      return delegator || attributes;
    };

    Model.prototype.serialize = function(model) {
      return serializeAttributes(this, this.getAttributes());
    };

    return Model;

  })(Backbone.Model);

  module.exports = Model;
  
}});

window.require.define({"lib/router": function(exports, require, module) {
  var HomePageController, JobsController, ProviderHomeController, ResumeController, Router, SeekerController, mediator,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  mediator = require('lib/mediator');

  ProviderHomeController = require('controller/provider_home');

  ResumeController = require('controller/resume');

  JobsController = require('controller/jobs');

  HomePageController = require('controller/home');

  SeekerController = require('controller/seeker');

  module.exports = Router = (function(_super) {

    __extends(Router, _super);

    function Router() {
      return Router.__super__.constructor.apply(this, arguments);
    }

    Router.prototype.initialize = function(options) {
      Router.__super__.initialize.apply(this, arguments);
      this.homepage = new HomePageController;
      this.provider = new ProviderHomeController;
      this.resumePage = new ResumeController;
      this.jobPage = new JobsController;
      return this.seekerPage = new SeekerController;
    };

    Router.prototype.routes = {
      '': 'home',
      'seeker/:id': 'seeker_home',
      'provider/:id': 'provider_home',
      'jobs/:id': 'jobs',
      'resume/:id': 'resume',
      'callback': 'callback'
    };

    Router.prototype.callback = function(options) {
      console.log(options);
      mediator.publish('login', this);
      mediator.publish('loginSuccess', this);
      return console.log(window.opener.application);
    };

    Router.prototype.home = function() {
      var options;
      options = {
        a: 10
      };
      return this.homepage.render(options);
    };

    Router.prototype.provider_home = function(id) {
      var options;
      options = {
        id: id
      };
      return this.provider.render(options);
    };

    Router.prototype.resume = function(id) {
      var options;
      options = {
        id: id
      };
      return this.resumePage.home(options);
    };

    Router.prototype.jobs = function(id) {
      var options;
      options = {
        id: id
      };
      return this.jobPage.home(options);
    };

    Router.prototype.seeker = function(id) {
      var options;
      console.log("[Router] Seeker " + id);
      options = {
        id: id
      };
      return this.seekerPage.home(options);
    };

    return Router;

  })(Backbone.Router);
  
}});

window.require.define({"lib/subscriber": function(exports, require, module) {
  var Subscriber, mediator, support;

  support = require('./support');

  mediator = require('./mediator');

  Subscriber = {
    subscribeEvent: function(type, handler) {
      if (typeof type !== 'string') {
        throw new TypeError('Subscriber#subscribeEvent: ' + 'type argument must be a string');
      }
      if (typeof handler !== 'function') {
        throw new TypeError('Subscriber#subscribeEvent: ' + 'handler argument must be a function');
      }
      mediator.unsubscribe(type, handler, this);
      return mediator.subscribe(type, handler, this);
    },
    unsubscribeEvent: function(type, handler) {
      if (typeof type !== 'string') {
        throw new TypeError('Subscriber#unsubscribeEvent: ' + 'type argument must be a string');
      }
      if (typeof handler !== 'function') {
        throw new TypeError('Subscriber#unsubscribeEvent: ' + 'handler argument must be a function');
      }
      return mediator.unsubscribe(type, handler);
    },
    unsubscribeAllEvents: function() {
      return mediator.unsubscribe(null, null, this);
    }
  };

  if (typeof Object.freeze === "function") {
    Object.freeze(Subscriber);
  }

  module.exports = Subscriber;
  
}});

window.require.define({"lib/support": function(exports, require, module) {
  var support;

  support = {
    propertyDescriptors: (function() {
      var o;
      if (!(typeof Object.defineProperty === 'function' && typeof Object.defineProperties === 'function')) {
        return false;
      }
      try {
        o = {};
        Object.defineProperty(o, 'foo', {
          value: 'bar'
        });
        return o.foo === 'bar';
      } catch (error) {
        return false;
      }
    })()
  };

  module.exports = support;
  
}});

window.require.define({"lib/utils": function(exports, require, module) {
  var support, utils,
    __slice = [].slice;

  support = require('lib/support');

  module.exports = utils = {
    beget: (function() {
      var ctor;
      if (typeof Object.create === 'function') {
        return Object.create;
      } else {
        ctor = function() {};
        return function(obj) {
          ctor.prototype = obj;
          return new ctor;
        };
      }
    })(),
    readonly: (function() {
      var readonlyDescriptor;
      if (support.propertyDescriptors) {
        readonlyDescriptor = {
          writable: false,
          enumerable: true,
          configurable: false
        };
        return function() {
          var obj, prop, properties, _i, _len;
          obj = arguments[0], properties = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
          for (_i = 0, _len = properties.length; _i < _len; _i++) {
            prop = properties[_i];
            Object.defineProperty(obj, prop, readonlyDescriptor);
          }
          return true;
        };
      } else {
        return function() {
          return false;
        };
      }
    })()
  };

  if (typeof Object.seal === "function") {
    Object.seal(utils);
  }
  
}});

window.require.define({"lib/view_helper": function(exports, require, module) {
  
  Handlebars.registerHelper('replace', function(context, options) {
    var hash, result, tag;
    hash = options.hash;
    if (hash['inContext'] === "true") {
      tag = hash['with'];
      result = context.replace('{{' + hash['what'] + '}}', this[tag]);
    } else {
      result = context.replace('{{' + hash['what'] + '}}', hash['with']);
    }
    return result;
  });
  
}});

window.require.define({"lib/views/collection_view": function(exports, require, module) {
  var CollectionView, View,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  View = require('./view');

  module.exports = CollectionView = (function(_super) {

    __extends(CollectionView, _super);

    function CollectionView() {
      this.renderAllItems = __bind(this.renderAllItems, this);

      this.itemsResetted = __bind(this.itemsResetted, this);

      this.itemRemoved = __bind(this.itemRemoved, this);

      this.itemAdded = __bind(this.itemAdded, this);
      return CollectionView.__super__.constructor.apply(this, arguments);
    }

    CollectionView.prototype.viewsByCid = null;

    CollectionView.prototype.visibleItems = null;

    CollectionView.prototype.listSelector = null;

    CollectionView.prototype.$list = null;

    CollectionView.prototype.initialize = function(options) {
      if (options == null) {
        options = {};
      }
      CollectionView.__super__.initialize.apply(this, arguments);
      _(options).defaults({
        render: true,
        renderItems: true,
        filterer: null
      });
      if (options.itemView != null) {
        this.itemView = options.itemView;
      }
      this.viewsByCid = {};
      this.visibleItems = [];
      this.addCollectionListeners();
      if (options.render) {
        this.render();
      }
      if (options.renderItems) {
        return this.renderAllItems();
      }
    };

    CollectionView.prototype.getView = function(model) {
      if (this.itemView != null) {
        return new this.itemView({
          model: model
        });
      } else {
        throw new Error('The CollectionView#itemView property must be\
  defined (or the getView() must be overridden)');
      }
    };

    CollectionView.prototype.addCollectionListeners = function() {
      this.modelBind('add', this.itemAdded);
      this.modelBind('remove', this.itemRemoved);
      return this.modelBind('reset', this.itemsResetted);
    };

    CollectionView.prototype.render = function() {
      CollectionView.__super__.render.apply(this, arguments);
      return this.$list = this.listSelector ? this.$(this.listSelector) : this.$el;
    };

    CollectionView.prototype.itemAdded = function(item, options) {
      if (options == null) {
        options = {};
      }
      console.log(item);
      console.log("item added");
      return this.renderAndInsertItem(item, options.index);
    };

    CollectionView.prototype.itemRemoved = function(item) {
      return console.log("Remove item");
    };

    CollectionView.prototype.itemsResetted = function() {
      console.log("Render all items " + this.constructor.name);
      return this.renderAllItems();
    };

    CollectionView.prototype.renderAllItems = function() {
      var cid, index, item, items, remainingViewsByCid, view, _i, _j, _len, _len1, _ref;
      items = this.collection.models;
      this.visibleItems = [];
      remainingViewsByCid = {};
      for (_i = 0, _len = items.length; _i < _len; _i++) {
        item = items[_i];
        view = this.viewsByCid[item.cid];
        if (view) {
          remainingViewsByCid[item.cid] = view;
        }
      }
      _ref = this.viewsByCid;
      for (cid in _ref) {
        if (!__hasProp.call(_ref, cid)) continue;
        view = _ref[cid];
        if (!(cid in remainingViewsByCid)) {
          this.removeView(cid, view);
        }
      }
      for (index = _j = 0, _len1 = items.length; _j < _len1; index = ++_j) {
        item = items[index];
        view = this.viewsByCid[item.cid];
        if (view) {
          this.insertView(item, view, index, 0);
        } else {
          this.renderAndInsertItem(item, index);
        }
      }
      if (!items.length) {
        return this.trigger('visibilityChange', this.visibleItems);
      }
    };

    CollectionView.prototype.renderAndInsertItem = function(item, index) {
      var view;
      view = this.renderItem(item);
      return this.insertView(item, view, index);
    };

    CollectionView.prototype.renderItem = function(item) {
      var view;
      view = this.viewsByCid[item.cid];
      if (!view) {
        view = this.getView(item);
        this.viewsByCid[item.cid] = view;
      }
      view.render();
      return view;
    };

    CollectionView.prototype.insertView = function(item, view, index, animationDuration) {
      var $list, $next, $previous, $viewEl, children, length, position, viewEl;
      if (index == null) {
        index = null;
      }
      if (animationDuration == null) {
        animationDuration = this.animationDuration;
      }
      position = typeof index === 'number' ? index : this.collection.indexOf(item);
      viewEl = view.el;
      $viewEl = view.$el;
      $list = this.$list;
      children = $list.children(this.itemSelector || void 0);
      length = children.length;
      if (length === 0 || position === length) {
        $list.append(viewEl);
      } else {
        if (position === 0) {
          $next = children.eq(position);
          $next.before(viewEl);
        } else {
          $previous = children.eq(position - 1);
          $previous.after(viewEl);
        }
      }
      return view.trigger('addedToDOM');
    };

    CollectionView.prototype.removeView = function(cid, view) {
      view.dispose();
      return delete this.viewsByCid[cid];
    };

    CollectionView.prototype.dispose = function() {
      var cid, prop, properties, view, _i, _len, _ref;
      if (this.disposed) {
        return;
      }
      _ref = this.viewsByCid;
      for (cid in _ref) {
        if (!__hasProp.call(_ref, cid)) continue;
        view = _ref[cid];
        view.dispose();
      }
      properties = ['$list', '$fallback', '$loading', 'viewsByCid', 'visibleItems'];
      for (_i = 0, _len = properties.length; _i < _len; _i++) {
        prop = properties[_i];
        delete this[prop];
      }
      return CollectionView.__super__.dispose.apply(this, arguments);
    };

    return CollectionView;

  })(View);
  
}});

window.require.define({"lib/views/layout": function(exports, require, module) {
  var Layout, Subscriber, mediator;

  mediator = require('lib/mediator');

  Subscriber = require('lib/subscriber');

  Layout = (function() {

    _.extend(Layout.prototype, Subscriber);

    Layout.prototype.title = '';

    Layout.prototype.el = document;

    Layout.prototype.$el = $(document);

    function Layout() {
      this.initialize.apply(this, arguments);
    }

    Layout.prototype.initialize = function(options) {
      return this.title = options.title;
    };

    Layout.prototype.disposed = false;

    Layout.prototype.dispose = function() {
      if (this.disposed) {
        return;
      }
      this.unsubscribeAllEvents();
      delete this.title;
      this.disposed = true;
      return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
    };

    return Layout;

  })();

  module.exports = Layout;
  
}});

window.require.define({"lib/views/view": function(exports, require, module) {
  var Subscriber, View,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  require('lib/view_helper');

  Subscriber = require('lib/subscriber');

  module.exports = View = (function(_super) {

    __extends(View, _super);

    function View() {
      this.render = __bind(this.render, this);

      this.addSubView = __bind(this.addSubView, this);
      return View.__super__.constructor.apply(this, arguments);
    }

    _(View.prototype).extend(Subscriber);

    View.prototype.autoRender = false;

    View.prototype.hasSubView = false;

    View.prototype.subViews = null;

    View.prototype.subViewsByName = null;

    View.prototype.filter = false;

    View.prototype.container = null;

    View.prototype.containerMethod = 'html';

    View.prototype.initialize = function(options) {
      if (options == null) {
        options = {};
      }
      _.extend(this, options);
      if (this.hasSubView) {
        this.subViews = [];
        this.subViewsByName = {};
      }
      if (this.autoRender) {
        return this.render();
      }
    };

    View.prototype.addSubView = function(name, view, where, attr) {
      var options, v;
      if (attr == null) {
        attr = null;
      }
      options = {
        el: where
      };
      if (attr) {
        options = _.extend({}, options, attr);
      }
      v = new view(options);
      v.parent = this;
      this.subViews.push(v);
      this.subViewsByName[name] = v;
      return v;
    };

    View.prototype.template = function() {};

    View.prototype.getTemplateData = function() {
      var items, model, templateData, _i, _len, _ref;
      if (this.model) {
        templateData = this.model.serialize();
      } else if (this.collection) {
        items = [];
        _ref = this.collection.models;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          model = _ref[_i];
          items.push(model.serialize());
        }
        templateData = {
          items: items
        };
      } else {
        templateData = {};
      }
      return templateData;
    };

    View.prototype.getRenderData = function() {};

    View.prototype.render = function(attr) {
      var data, html;
      if (attr == null) {
        attr = null;
      }
      this.beforeRender();
      console.debug("Rendering " + this.constructor.name);
      data = this.getTemplateData();
      if (attr) {
        data = _.extend({}, data, attr);
      }
      if (this.filter) {
        data = this.filter(data);
      }
      html = this.template(data);
      this.$el.html(html);
      this.afterRender();
      return this;
    };

    View.prototype.beforeRender = function() {};

    View.prototype.afterRender = function() {
      if (this.container) {
        $(this.container)[this.containerMethod](this.el);
        this.trigger('addedToDom');
      }
      return this;
    };

    View.prototype.delegate = function(eventType, second, third) {
      var handler, selector;
      if (typeof eventType !== 'string') {
        throw new TypeError('View#delegate: first argument must be a string');
      }
      if (arguments.length === 2) {
        handler = second;
      } else if (arguments.length === 3) {
        selector = second;
        if (typeof selector !== 'string') {
          throw new TypeError('View#delegate: ' + 'second argument must be a string');
        }
        handler = third;
      } else {
        throw new TypeError('View#delegate: ' + 'only two or three arguments are allowed');
      }
      if (typeof handler !== 'function') {
        throw new TypeError('View#delegate: ' + 'handler argument must be function');
      }
      eventType += ".delegate" + this.cid;
      handler = _(handler).bind(this);
      if (selector) {
        this.$el.on(eventType, selector, handler);
      } else {
        this.$el.on(eventType, handler);
      }
      return handler;
    };

    View.prototype.undelegate = function() {
      return this.$el.unbind(".delegate" + this.cid);
    };

    View.prototype.modelBind = function(type, handler) {
      var modelOrCollection;
      if (typeof type !== 'string') {
        throw new TypeError('View#modelBind: ' + 'type must be a string');
      }
      if (typeof handler !== 'function') {
        throw new TypeError('View#modelBind: ' + 'handler argument must be function');
      }
      modelOrCollection = this.model || this.collection;
      if (!modelOrCollection) {
        throw new TypeError('View#modelBind: no model or collection set');
      }
      modelOrCollection.off(type, handler, this);
      return modelOrCollection.on(type, handler, this);
    };

    View.prototype.modelUnbind = function(type, handler) {
      var modelOrCollection;
      if (typeof type !== 'string') {
        throw new TypeError('View#modelUnbind: ' + 'type argument must be a string');
      }
      if (typeof handler !== 'function') {
        throw new TypeError('View#modelUnbind: ' + 'handler argument must be a function');
      }
      modelOrCollection = this.model || this.collection;
      if (!modelOrCollection) {
        return;
      }
      return modelOrCollection.off(type, handler);
    };

    View.prototype.modelUnbindAll = function() {
      var modelOrCollection;
      modelOrCollection = this.model || this.collection;
      if (!modelOrCollection) {
        return;
      }
      return modelOrCollection.off(null, null, this);
    };

    View.prototype.disposed = false;

    View.prototype.dispose = function() {
      var prop, properties, _i, _len;
      if (this.disposed) {
        return;
      }
      this.unsubscribeAllEvents();
      this.modelUnbindAll();
      this.off();
      this.$el.remove();
      properties = ['el', '$el', 'options', 'model', 'collection', 'subviews', 'subviewsByName', '_callbacks'];
      for (_i = 0, _len = properties.length; _i < _len; _i++) {
        prop = properties[_i];
        delete this[prop];
      }
      this.disposed = true;
      return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
    };

    return View;

  })(Backbone.View);
  
}});

window.require.define({"models/breadcrum": function(exports, require, module) {
  var BreadCrum, Model,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Model = require("lib/models/model");

  BreadCrum = (function(_super) {

    __extends(BreadCrum, _super);

    function BreadCrum() {
      return BreadCrum.__super__.constructor.apply(this, arguments);
    }

    return BreadCrum;

  })(Model);

  module.exports = BreadCrum;
  
}});

window.require.define({"models/home": function(exports, require, module) {
  var Collection, Job, JobList, Model,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Model = require("lib/models/model");

  Collection = require("lib/models/collection");

  Job = (function(_super) {

    __extends(Job, _super);

    function Job() {
      return Job.__super__.constructor.apply(this, arguments);
    }

    Job.prototype.defaults = {
      ntags: 0,
      mails: 0,
      resumes: 0,
      applicant: 0,
      resources: {
        mail: '#mail/{{id}}/new',
        resume: '#resume/{{id}}/new',
        applicant: '#applicant/{{id}}/new'
      }
    };

    Job.prototype.initialize = function(options) {
      var today;
      Job.__super__.initialize.apply(this, arguments);
      today = new Date();
      return {
        date: {
          day: today.getDay(),
          month: today.getMonth(),
          year: today.getYear()
        }
      };
    };

    return Job;

  })(Model);

  JobList = (function(_super) {

    __extends(JobList, _super);

    function JobList() {
      return JobList.__super__.constructor.apply(this, arguments);
    }

    JobList.prototype.url = 'data/home.json';

    JobList.prototype.model = Job;

    JobList.prototype.initialize = function() {
      return JobList.__super__.initialize.apply(this, arguments);
    };

    return JobList;

  })(Collection);

  exports.Collection = JobList;
  
}});

window.require.define({"models/jobs": function(exports, require, module) {
  var JobDetail, Model,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Model = require("lib/models/model");

  JobDetail = (function(_super) {

    __extends(JobDetail, _super);

    function JobDetail() {
      this.url = __bind(this.url, this);
      return JobDetail.__super__.constructor.apply(this, arguments);
    }

    JobDetail.prototype.urlRoot = 'data/jobs';

    JobDetail.prototype.url = function() {
      var base;
      base = this.urlRoot + '/' + this.id + '.json';
      return base;
    };

    return JobDetail;

  })(Model);

  module.exports = JobDetail;
  
}});

window.require.define({"models/page": function(exports, require, module) {
  var Model, PageModel,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Model = require("lib/models/model");

  PageModel = (function(_super) {

    __extends(PageModel, _super);

    function PageModel() {
      this.url = __bind(this.url, this);
      return PageModel.__super__.constructor.apply(this, arguments);
    }

    PageModel.prototype.urlRoot = 'data/page';

    PageModel.prototype.url = function() {
      var base;
      base = this.urlRoot + '/' + this.id + '.json';
      return base;
    };

    return PageModel;

  })(Model);

  module.exports = PageModel;
  
}});

window.require.define({"models/resume": function(exports, require, module) {
  var Collection, Model, Resume, ResumeList,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Model = require("lib/models/model");

  Collection = require("lib/models/collection");

  Resume = (function(_super) {

    __extends(Resume, _super);

    function Resume() {
      return Resume.__super__.constructor.apply(this, arguments);
    }

    return Resume;

  })(Model);

  ResumeList = (function(_super) {

    __extends(ResumeList, _super);

    function ResumeList() {
      return ResumeList.__super__.constructor.apply(this, arguments);
    }

    ResumeList.prototype.url = '/resume.json';

    ResumeList.prototype.model = Resume;

    return ResumeList;

  })(Collection);

  module.exports = new ResumeList;
  
}});

window.require.define({"views/breadcrum_view": function(exports, require, module) {
  var BreadCrumView, View, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  View = require('lib/views/view');

  template = require('./templates/breadcrum');

  BreadCrumView = (function(_super) {

    __extends(BreadCrumView, _super);

    function BreadCrumView() {
      return BreadCrumView.__super__.constructor.apply(this, arguments);
    }

    BreadCrumView.prototype.id = "breadcrum-view";

    BreadCrumView.prototype.template = template;

    BreadCrumView.prototype.container = '#breadcrum';

    BreadCrumView.prototype.autoRender = true;

    return BreadCrumView;

  })(View);

  module.exports = BreadCrumView;
  
}});

window.require.define({"views/home_view": function(exports, require, module) {
  var HomePageView, View, template,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  View = require("lib/views/view");

  template = require('views/templates/home_page');

  HomePageView = (function(_super) {

    __extends(HomePageView, _super);

    function HomePageView() {
      this.addAuthSuccess = __bind(this.addAuthSuccess, this);

      this.addAuthFail = __bind(this.addAuthFail, this);

      this.addLoading = __bind(this.addLoading, this);
      return HomePageView.__super__.constructor.apply(this, arguments);
    }

    HomePageView.prototype.id = "home-view";

    HomePageView.prototype.template = template;

    HomePageView.prototype.className = 'span12';

    HomePageView.prototype.container = '#magic';

    HomePageView.prototype.hasSubView = true;

    HomePageView.prototype.autoRender = true;

    HomePageView.prototype.initialize = function(options) {
      HomePageView.__super__.initialize.apply(this, arguments);
      this.twiiterbutton = $('button#twitterlogin');
      this.delegate('click', 'button#twitterlogin', this.addLoading);
      this.subscribeEvent('!loginFail', this.addAuthFail);
      return this.subscribeEvent('loginSuccess', this.addAuthSuccess);
    };

    HomePageView.prototype.addLoading = function(event) {
      var target;
      target = event.target;
      return $(target).html('Authenticating ....');
    };

    HomePageView.prototype.addAuthFail = function(options) {
      return console.log(options);
    };

    HomePageView.prototype.addAuthSuccess = function(options) {
      console.log(options);
      return this.twitterbutton.html('Welcome Vishwas');
    };

    return HomePageView;

  })(View);

  module.exports = HomePageView;
  
}});

window.require.define({"views/jobs_view": function(exports, require, module) {
  var DataView, JobModel, JobsView, View, dataTemplate, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  View = require("lib/views/view");

  template = require('views/templates/jobs');

  JobModel = require('models/jobs');

  dataTemplate = require('views/templates/jobs/data');

  DataView = (function(_super) {

    __extends(DataView, _super);

    function DataView() {
      return DataView.__super__.constructor.apply(this, arguments);
    }

    DataView.prototype.id = "data-view";

    DataView.prototype.template = dataTemplate;

    DataView.prototype.initialize = function() {
      this.model = new JobModel({
        id: 21
      });
      this.modelBind('change', this.changeRender);
      return this.model.fetch();
    };

    DataView.prototype.changeRender = function(options) {
      return this.render();
    };

    return DataView;

  })(View);

  JobsView = (function(_super) {

    __extends(JobsView, _super);

    function JobsView() {
      return JobsView.__super__.constructor.apply(this, arguments);
    }

    JobsView.prototype.id = "jobs-view";

    JobsView.prototype.template = template;

    JobsView.prototype.className = 'span12';

    JobsView.prototype.container = '#magic';

    JobsView.prototype.hasSubView = true;

    JobsView.prototype.autoRender = true;

    JobsView.prototype.afterRender = function() {
      JobsView.__super__.afterRender.apply(this, arguments);
      this.dataView = this.addSubView('data', DataView, this.$('#data'));
      return this;
    };

    return JobsView;

  })(View);

  module.exports = JobsView;
  
}});

window.require.define({"views/layout": function(exports, require, module) {
  var Layout, LayoutView,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  LayoutView = require('lib/views/layout');

  Layout = (function(_super) {

    __extends(Layout, _super);

    function Layout() {
      return Layout.__super__.constructor.apply(this, arguments);
    }

    return Layout;

  })(LayoutView);
  
}});

window.require.define({"views/provider_home_view": function(exports, require, module) {
  var CollectionView, HeaderView, HomeView, JobView, JobsView, NewJobView, OptionsView, View, collection, headerTemplate, home, jobTemplate, jobsTemplate, mediator, newJobTemplate, optionsTemplate, template,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  View = require('lib/views/view');

  CollectionView = require('lib/views/collection_view');

  mediator = require('lib/mediator');

  template = require('./templates/home');

  headerTemplate = require('./templates/home/header');

  newJobTemplate = require('./templates/home/newjob');

  optionsTemplate = require('./templates/home/options');

  jobsTemplate = require('./templates/home/jobs');

  home = require('models/home');

  jobTemplate = require('./templates/home/job');

  JobView = (function(_super) {

    __extends(JobView, _super);

    function JobView() {
      this.afterRender = __bind(this.afterRender, this);

      this.checkState = __bind(this.checkState, this);

      this.eventChecked = __bind(this.eventChecked, this);

      this.evtSelectAll = __bind(this.evtSelectAll, this);
      return JobView.__super__.constructor.apply(this, arguments);
    }

    JobView.prototype.template = jobTemplate;

    JobView.prototype.tagName = 'tr';

    JobView.prototype.isChecked = false;

    JobView.prototype.initialize = function(options) {
      JobView.__super__.initialize.apply(this, arguments);
      this.subscribeEvent('optionTrigger', this.optionTrigger);
      this.delegate('click', 'input:checkbox', this.eventChecked);
      return this.subscribeEvent('optSelectAll', this.evtSelectAll);
    };

    JobView.prototype.evtSelectAll = function(data) {
      return this.toggleCheckbox(data);
    };

    JobView.prototype.toggleCheckbox = function(state) {
      return this.$('input:checkbox').prop("checked", state);
    };

    JobView.prototype.eventChecked = function(events) {
      var target;
      target = events.target;
      this.isChecked = this.checkState();
      return this.toggleCheckbox(this.isChecked);
    };

    JobView.prototype.checkState = function() {
      var target;
      target = this.$('input:checked');
      return target.is(':checked');
    };

    JobView.prototype.optionTrigger = function(options) {
      console.log(options);
      if (!this.checkState()) {
        return;
      }
      return console.log(this);
    };

    JobView.prototype.afterRender = function() {
      JobView.__super__.afterRender.apply(this, arguments);
      return this.$('a[rel=tooltip]').tooltip().click(function(e) {
        return e.preventDefault();
      });
    };

    return JobView;

  })(View);

  collection = new home.Collection;

  JobsView = (function(_super) {

    __extends(JobsView, _super);

    function JobsView() {
      this.addModelView = __bind(this.addModelView, this);
      return JobsView.__super__.constructor.apply(this, arguments);
    }

    JobsView.prototype.template = jobsTemplate;

    JobsView.prototype.collection = collection;

    JobsView.prototype.tagName = 'tbody';

    JobsView.prototype.listSelector = 'tbody';

    JobsView.prototype.itemView = JobView;

    JobsView.prototype.initialize = function() {
      JobsView.__super__.initialize.apply(this, arguments);
      this.subscribeEvent('newJobAdded', this.addModelView);
      return this.collection.fetch();
    };

    JobsView.prototype.addModelView = function(options) {
      var result, today;
      today = new Date();
      result = {
        id: 23,
        name: 'test',
        description: 'tsejldfajaljf lajfj lk fsdjklsdf ajkl',
        meta: {
          date: {
            day: today.getDate(),
            month: today.getMonth() + 1,
            year: today.getFullYear()
          }
        }
      };
      return this.collection.create(result);
    };

    return JobsView;

  })(CollectionView);

  OptionsView = (function(_super) {

    __extends(OptionsView, _super);

    function OptionsView() {
      this.pushOptions = __bind(this.pushOptions, this);

      this.eventButtonDelete = __bind(this.eventButtonDelete, this);

      this.eventButtonSetting = __bind(this.eventButtonSetting, this);

      this.eventSelectAll = __bind(this.eventSelectAll, this);
      return OptionsView.__super__.constructor.apply(this, arguments);
    }

    OptionsView.prototype.id = 'options';

    OptionsView.prototype.template = optionsTemplate;

    OptionsView.prototype.autoRender = true;

    OptionsView.prototype.initialize = function(options) {
      OptionsView.__super__.initialize.apply(this, arguments);
      this.delegate('click', '#selectAll', this.eventSelectAll);
      this.delegate('click', '#setting', this.eventButtonSetting);
      return this.delegate('click', '#delete', this.eventButtonDelete);
    };

    OptionsView.prototype.eventSelectAll = function(event) {
      var result, target;
      result = false;
      target = event.target;
      if ($(target).is(':checked')) {
        result = true;
      }
      return mediator.publish('optSelectAll', result);
    };

    OptionsView.prototype.eventButtonSetting = function(events) {
      return this.pushOptions(events);
    };

    OptionsView.prototype.eventButtonDelete = function(events) {
      return this.pushOptions(events);
    };

    OptionsView.prototype.pushOptions = function(events) {
      return mediator.publish('optionTrigger', $(events.target));
    };

    return OptionsView;

  })(View);

  NewJobView = (function(_super) {

    __extends(NewJobView, _super);

    function NewJobView() {
      this.onSubmit = __bind(this.onSubmit, this);
      return NewJobView.__super__.constructor.apply(this, arguments);
    }

    NewJobView.prototype.id = 'new-job';

    NewJobView.prototype.template = newJobTemplate;

    NewJobView.prototype.className = 'well';

    NewJobView.prototype.autoRender = true;

    NewJobView.prototype.initialize = function(options) {
      NewJobView.__super__.initialize.apply(this, arguments);
      this.delegate('click', 'button#addbutton', this.getForm);
      return this.delegate('submit', '#job-form', this.onSubmit);
    };

    NewJobView.prototype.getForm = function(e) {
      var target;
      target = e.target;
      return $(target).toggleClass('hide').siblings().toggleClass('hide');
    };

    NewJobView.prototype.toggleView = function(target) {
      return $(target).parent().toggleClass('hide').siblings().toggleClass('hide');
    };

    NewJobView.prototype.onSubmit = function(e) {
      var target;
      target = e.target;
      mediator.publish('newJobAdded', $(target));
      this.toggleView(target);
      return false;
    };

    return NewJobView;

  })(View);

  HeaderView = (function(_super) {

    __extends(HeaderView, _super);

    function HeaderView() {
      return HeaderView.__super__.constructor.apply(this, arguments);
    }

    HeaderView.prototype.id = 'header';

    HeaderView.prototype.template = headerTemplate;

    HeaderView.prototype.className = 'page-header';

    HeaderView.prototype.autoRender = true;

    return HeaderView;

  })(View);

  HomeView = (function(_super) {

    __extends(HomeView, _super);

    function HomeView() {
      return HomeView.__super__.constructor.apply(this, arguments);
    }

    HomeView.prototype.id = 'home-view';

    HomeView.prototype.template = template;

    HomeView.prototype.className = 'span12';

    HomeView.prototype.hasSubView = true;

    HomeView.prototype.container = '#magic';

    HomeView.prototype.autoRender = true;

    HomeView.prototype.afterRender = function() {
      HomeView.__super__.afterRender.apply(this, arguments);
      this.headerView = this.addSubView('header', HeaderView, this.$('#homeheader'));
      this.newJobView = this.addSubView('newJob', NewJobView, this.$('#new-job'));
      this.optionView = this.addSubView('options', OptionsView, this.$('#optionbox'));
      this.tableView = this.addSubView('jobs', JobsView, this.$('#jobsTable'));
      return this;
    };

    return HomeView;

  })(View);

  module.exports = HomeView;
  
}});

window.require.define({"views/resume_view": function(exports, require, module) {
  var AnnotationDetail, AnnotationsView, PageModel, PageView, PagesView, ResumeMainView, ResumeView, RightView, View, annotationsTemplate, detailTemplate, pageTemplate, pagesTemplate, resumeTemplate, rightTemplate, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  View = require('lib/views/view');

  template = require('views/templates/resume');

  resumeTemplate = require('views/templates/resume/main1');

  pageTemplate = require('views/templates/resume/page');

  pagesTemplate = require('views/templates/resume/pages');

  annotationsTemplate = require('views/templates/resume/annotations');

  detailTemplate = require('views/templates/resume/detail');

  rightTemplate = require('views/templates/resume/right');

  PageModel = require('models/page');

  AnnotationDetail = (function(_super) {

    __extends(AnnotationDetail, _super);

    function AnnotationDetail() {
      return AnnotationDetail.__super__.constructor.apply(this, arguments);
    }

    AnnotationDetail.prototype.template = detailTemplate;

    AnnotationDetail.prototype.autoRender = true;

    return AnnotationDetail;

  })(View);

  AnnotationsView = (function(_super) {

    __extends(AnnotationsView, _super);

    function AnnotationsView() {
      this.normalize = __bind(this.normalize, this);

      this.clicked = __bind(this.clicked, this);
      return AnnotationsView.__super__.constructor.apply(this, arguments);
    }

    AnnotationsView.prototype.id = 'annotations';

    AnnotationsView.prototype.basictemplate = annotationsTemplate;

    AnnotationsView.prototype.autoRender = true;

    AnnotationsView.prototype.hasSubView = true;

    AnnotationsView.prototype.toggleTemplate = detailTemplate;

    AnnotationsView.prototype.isDetailed = false;

    AnnotationsView.prototype.filter = function(data) {
      var result,
        _this = this;
      result = _.filter(data['annotations'], function(annotation) {
        if (annotation['page'] === _this.pagenumber) {
          return annotation;
        }
      });
      return {
        "annotations": result
      };
    };

    AnnotationsView.prototype.initialize = function() {
      this.template = this.basictemplate;
      return AnnotationsView.__super__.initialize.apply(this, arguments);
    };

    AnnotationsView.prototype.events = {
      'click .annotationWrapper': 'clicked'
    };

    AnnotationsView.prototype.clicked = function(events) {
      if (this.isDetailed) {
        this.template = this.basictemplate;
        this.isDetailed = false;
      } else {
        this.template = this.toggleTemplate;
        this.isDetailed = true;
      }
      this.render();
      return events.preventDefault();
    };

    AnnotationsView.prototype.normalize = function() {
      this.template = this.basictemplate;
      return this.render();
    };

    return AnnotationsView;

  })(View);

  PageView = (function(_super) {

    __extends(PageView, _super);

    function PageView() {
      return PageView.__super__.constructor.apply(this, arguments);
    }

    PageView.prototype.template = pageTemplate;

    PageView.prototype.hasSubView = true;

    PageView.prototype.containerMethod = 'append';

    PageView.prototype.initialize = function(options) {
      PageView.__super__.initialize.apply(this, arguments);
      return this.render({
        pagenumber: this.pagenumber
      });
    };

    PageView.prototype.render = function(attr) {
      var param;
      PageView.__super__.render.apply(this, arguments);
      param = _.extend({}, attr, {
        model: this.model
      });
      if (this.hasAnnotations) {
        this.annotationsView = this.addSubView('annotations', AnnotationsView, this.$('#annotations'), param);
      }
      return this;
    };

    return PageView;

  })(View);

  PagesView = (function(_super) {

    __extends(PagesView, _super);

    function PagesView() {
      return PagesView.__super__.constructor.apply(this, arguments);
    }

    PagesView.prototype.id = 'pagesbig';

    PagesView.prototype.template = pagesTemplate;

    PagesView.prototype.autoRender = true;

    PagesView.prototype.hasSubView = true;

    PagesView.prototype.render = function() {
      var annotations, context, i, meta, pages, pagesWithAnno, param, view, _i;
      PagesView.__super__.render.apply(this, arguments);
      pages = this.model.get('pages');
      context = {
        model: this.model
      };
      annotations = this.model.get('annotations');
      pagesWithAnno = _.pluck(annotations, 'page');
      for (i = _i = 1; 1 <= pages ? _i <= pages : _i >= pages; i = 1 <= pages ? ++_i : --_i) {
        meta = {
          pagenumber: i
        };
        if (__indexOf.call(pagesWithAnno, i) >= 0) {
          meta['hasAnnotations'] = true;
        } else {
          meta['hasAnnotations'] = false;
        }
        param = _.extend({}, context, meta);
        view = this.addSubView('page', PageView, this.$('#pagesbig'), param);
      }
      return this;
    };

    return PagesView;

  })(View);

  RightView = (function(_super) {

    __extends(RightView, _super);

    function RightView() {
      return RightView.__super__.constructor.apply(this, arguments);
    }

    RightView.prototype.template = rightTemplate;

    RightView.prototype.autoRender = true;

    return RightView;

  })(View);

  ResumeView = (function(_super) {

    __extends(ResumeView, _super);

    function ResumeView() {
      this.clicked = __bind(this.clicked, this);

      this.renderSubViews = __bind(this.renderSubViews, this);

      this.changeModel = __bind(this.changeModel, this);
      return ResumeView.__super__.constructor.apply(this, arguments);
    }

    ResumeView.prototype.id = 'pages';

    ResumeView.prototype.template = resumeTemplate;

    ResumeView.prototype.hasSubView = true;

    ResumeView.prototype.initialize = function() {
      this.model = new PageModel({
        id: 21
      });
      this.modelBind('change', this.changeModel);
      this.model.fetch();
      return ResumeView.__super__.initialize.apply(this, arguments);
    };

    ResumeView.prototype.changeModel = function(options) {
      return this.renderSubViews({
        model: options
      });
    };

    ResumeView.prototype.events = {
      'click #pages': 'clicked'
    };

    ResumeView.prototype.renderSubViews = function(options) {
      this.render();
      this.pageView = this.addSubView('page', PagesView, this.$('#pages'), options);
      return this.rightView = this.addSubView('rigth', RightView, this.$('#rightdata'), options);
    };

    ResumeView.prototype.clicked = function(events) {
      this.pageView.annotationsView.normalize();
      return events.stopPropagation();
    };

    return ResumeView;

  })(View);

  ResumeMainView = (function(_super) {

    __extends(ResumeMainView, _super);

    function ResumeMainView() {
      return ResumeMainView.__super__.constructor.apply(this, arguments);
    }

    ResumeMainView.prototype.id = "resume-view";

    ResumeMainView.prototype.template = template;

    ResumeMainView.prototype.className = 'span12';

    ResumeMainView.prototype.hasSubView = true;

    ResumeMainView.prototype.container = "#magic";

    ResumeMainView.prototype.autoRender = true;

    ResumeMainView.prototype.afterRender = function() {
      ResumeMainView.__super__.afterRender.apply(this, arguments);
      this.resumeView = this.addSubView('resume', ResumeView, this.$('#resumeContainer'));
      return this;
    };

    return ResumeMainView;

  })(View);

  module.exports = ResumeMainView;
  
}});

window.require.define({"views/templates/breadcrum": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<ul class=\"breadcrumb\">\n  <li>\n    <a href=\"#\">Home</a> <span class=\"divider\">/</span>\n  </li>\n  <li>\n    <a href=\"#\">Opening</a> <span class=\"divider\">/</span>\n  </li>\n  <li class=\"active\">Php Developer</li>\n</ul>\n";});
}});

window.require.define({"views/templates/home": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<div class=\"HV-padding HV-whitebg HV-border\">\n  <!--Yahan par headerView ka content render hoga-->\n  <div id=\"homeheader\" class=\"page-header\"></div>\n  <!--Yahan par new job ka view aayega-->\n  <div class=\"well\" id=\"new-job\"></div>\n  <!--Yahan par OptionsView ka content render hoga-->\n  <div id=\"optionbox\" style=\"padding-left : 5px\"></div>\n  <!--Yahan par jobs ke table jayegi-->\n  <table id=\"jobsTable\" class=\"table table-condensed HV-topmargin HV-openingtable\"></table>\n</div>\n";});
}});

window.require.define({"views/templates/home/header": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<h1>Opening</h1>\n";});
}});

window.require.define({"views/templates/home/job": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, stack3, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n    ";
    foundHelper = helpers.status;
    stack1 = foundHelper || depth0.status;
    stack2 = helpers['with'];
    tmp1 = self.program(2, program2, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n  ";
    return buffer;}
  function program2(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n      <span class=\"label ";
    foundHelper = helpers.style;
    stack1 = foundHelper || depth0.style;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "style", { hash: {} }); }
    buffer += escapeExpression(stack1) + " HV-rightmargin\">";
    foundHelper = helpers.title;
    stack1 = foundHelper || depth0.title;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</span>\n    ";
    return buffer;}

  function program4(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += " \n";
    foundHelper = helpers.meta;
    stack1 = foundHelper || depth0.meta;
    stack2 = helpers['with'];
    tmp1 = self.program(5, program5, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    return buffer;}
  function program5(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n<td>\n  ";
    foundHelper = helpers.salary;
    stack1 = foundHelper || depth0.salary;
    stack2 = helpers['if'];
    tmp1 = self.program(6, program6, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n<td>\n\n";
    foundHelper = helpers.date;
    stack1 = foundHelper || depth0.date;
    stack2 = helpers['with'];
    tmp1 = self.program(10, program10, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "</td>\n\n";
    return buffer;}
  function program6(depth0,data) {
    
    var stack1, stack2;
    foundHelper = helpers.salary;
    stack1 = foundHelper || depth0.salary;
    stack2 = helpers['with'];
    tmp1 = self.program(7, program7, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { return stack1; }
    else { return ''; }}
  function program7(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "<span class=\"label";
    foundHelper = helpers.style;
    stack1 = foundHelper || depth0.style;
    stack2 = helpers['if'];
    tmp1 = self.program(8, program8, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\">Salary ";
    foundHelper = helpers.lowercap;
    stack1 = foundHelper || depth0.lowercap;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "lowercap", { hash: {} }); }
    buffer += escapeExpression(stack1) + "~";
    foundHelper = helpers.uppercap;
    stack1 = foundHelper || depth0.uppercap;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "uppercap", { hash: {} }); }
    buffer += escapeExpression(stack1) + " ";
    foundHelper = helpers.unit;
    stack1 = foundHelper || depth0.unit;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "unit", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</span></td>";
    return buffer;}
  function program8(depth0,data) {
    
    var buffer = "", stack1;
    buffer += " ";
    foundHelper = helpers.style;
    stack1 = foundHelper || depth0.style;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "style", { hash: {} }); }
    buffer += escapeExpression(stack1);
    return buffer;}

  function program10(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "<p>";
    foundHelper = helpers.day;
    stack1 = foundHelper || depth0.day;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "day", { hash: {} }); }
    buffer += escapeExpression(stack1) + "/";
    foundHelper = helpers.month;
    stack1 = foundHelper || depth0.month;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "month", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</p>";
    return buffer;}

    buffer += "<td><label class=\"checkbox\"><input type=\"checkbox\" name=\"job\" value=\"";
    foundHelper = helpers.id;
    stack1 = foundHelper || depth0.id;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "id", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\"></label></td>\n<td><p style=\"width : 200px;\" class=\"HV-text-overflow\"><a href=\"#jobs/";
    foundHelper = helpers.id;
    stack1 = foundHelper || depth0.id;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "id", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">";
    foundHelper = helpers.name;
    stack1 = foundHelper || depth0.name;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</a></p></td>\n<td>\n  <div class=\"pull-right\">\n    <a class=\"btn-small\" href=\"";
    foundHelper = helpers.resources;
    stack1 = foundHelper || depth0.resources;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.mail);
    stack2 = {};
    stack3 = "id";
    stack2['what'] = stack3;
    stack3 = "id";
    stack2['with'] = stack3;
    stack3 = "true";
    stack2['inContext'] = stack3;
    foundHelper = helpers.replace;
    stack3 = foundHelper || depth0.replace;
    tmp1 = {};
    tmp1.hash = stack2;
    if(typeof stack3 === functionType) { stack1 = stack3.call(depth0, stack1, tmp1); }
    else if(stack3=== undef) { stack1 = helperMissing.call(depth0, "replace", stack1, tmp1); }
    else { stack1 = stack3; }
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\" rel=\"tooltip\" data-original-title=\"";
    foundHelper = helpers.mails;
    stack1 = foundHelper || depth0.mails;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "mails", { hash: {} }); }
    buffer += escapeExpression(stack1) + " new Mail\">";
    foundHelper = helpers.mails;
    stack1 = foundHelper || depth0.mails;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "mails", { hash: {} }); }
    buffer += escapeExpression(stack1) + " <i class=\"icon-envelope\"></i></a></div>\n    </td><td>\n\n    <div class=\"pull-right\">\n      <a class=\"btn-small\" href=\"";
    foundHelper = helpers.resources;
    stack1 = foundHelper || depth0.resources;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.resume);
    stack2 = {};
    stack3 = "id";
    stack2['what'] = stack3;
    stack3 = "id";
    stack2['with'] = stack3;
    stack3 = "true";
    stack2['inContext'] = stack3;
    foundHelper = helpers.replace;
    stack3 = foundHelper || depth0.replace;
    tmp1 = {};
    tmp1.hash = stack2;
    if(typeof stack3 === functionType) { stack1 = stack3.call(depth0, stack1, tmp1); }
    else if(stack3=== undef) { stack1 = helperMissing.call(depth0, "replace", stack1, tmp1); }
    else { stack1 = stack3; }
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\" rel=\"tooltip\" data-original-title=\"";
    foundHelper = helpers.resumes;
    stack1 = foundHelper || depth0.resumes;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "resumes", { hash: {} }); }
    buffer += escapeExpression(stack1) + " new Resume\">";
    foundHelper = helpers.resumes;
    stack1 = foundHelper || depth0.resumes;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "resumes", { hash: {} }); }
    buffer += escapeExpression(stack1) + " <i class=\"icon-book\"></i></a></div>\n  </td><td>\n  <div class=\"pull-right\">\n    <a class=\"btn-small\" href=\"";
    foundHelper = helpers.resources;
    stack1 = foundHelper || depth0.resources;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.applicant);
    stack2 = {};
    stack3 = "id";
    stack2['what'] = stack3;
    stack3 = "id";
    stack2['with'] = stack3;
    stack3 = "true";
    stack2['inContext'] = stack3;
    foundHelper = helpers.replace;
    stack3 = foundHelper || depth0.replace;
    tmp1 = {};
    tmp1.hash = stack2;
    if(typeof stack3 === functionType) { stack1 = stack3.call(depth0, stack1, tmp1); }
    else if(stack3=== undef) { stack1 = helperMissing.call(depth0, "replace", stack1, tmp1); }
    else { stack1 = stack3; }
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\" rel=\"tooltip\" data-original-title=\"";
    foundHelper = helpers.applicant;
    stack1 = foundHelper || depth0.applicant;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "applicant", { hash: {} }); }
    buffer += escapeExpression(stack1) + " people applied\">";
    foundHelper = helpers.applicant;
    stack1 = foundHelper || depth0.applicant;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "applicant", { hash: {} }); }
    buffer += escapeExpression(stack1) + " <i class=\"icon-user\"></i></a></div>\n</td>\n<td><p class=\"HV-tabletext-width HV-text-overflow\">\n  ";
    foundHelper = helpers.status;
    stack1 = foundHelper || depth0.status;
    stack2 = helpers['if'];
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n  ";
    foundHelper = helpers.description;
    stack1 = foundHelper || depth0.description;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "description", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</p></td>\n\n";
    foundHelper = helpers.meta;
    stack1 = foundHelper || depth0.meta;
    stack2 = helpers['if'];
    tmp1 = self.program(4, program4, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n";
    return buffer;});
}});

window.require.define({"views/templates/home/jobs": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<!--<table id=\"homejobs\" class=\"table table-condensed HV-topmargin HV-openingtable\"></table>-->\n<tbody></tbody>\n";});
}});

window.require.define({"views/templates/home/jobstemp": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<tbody id=\"test\"></tbody>\n";});
}});

window.require.define({"views/templates/home/newjob": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<button id=\"addbutton\" class=\"btn btn-primary\" href=\"#\">Create a new Job</button>\n<div id=\"job-form\" class=\"hide\">\n  <form class=\"form-horizontal\">\n    <fieldset>\n      <div class=\"control-group\">\n        <label class=\"control-label\" for=\"input01\">Title</label>\n        <div class=\"controls\">\n          <input type=\"text\" class=\"input-xlarge\" id=\"input01\" placeholder=\"Web Developer..\">\n          <p class=\"help-block\">Who are you looking for ?</p>\n        </div>\n      </div>\n      <div class=\"control-group\">\n        <label class=\"control-label\" for=\"input02\">City</label>\n        <div class=\"controls\">\n          <input type=\"text\" class=\"input-xlarge\" id=\"input02\" placeholder=\"New Delhi\">\n          <p class=\"help-block\">For which city you are looking for ?</p>\n        </div>\n      </div>\n      <div class=\"control-group\">\n        <label class=\"control-label\" for=\"input03\">State</label>\n        <div class=\"controls\">\n          <input type=\"text\" class=\"input-xlarge\" id=\"input03\" placeholder=\"Delhi\">\n          <p class=\"help-block\">For which state you are looking for ?</p>\n        </div>\n      </div>\n      <div class=\"control-group\">\n        <label class=\"control-label\" for=\"input04\">Country</label>\n        <div class=\"controls\">\n          <input type=\"text\" class=\"input-xlarge\" id=\"input04\" placeholder=\"India\">\n          <p class=\"help-block\">Which country ?</p>\n        </div>\n      </div>\n    <div class=\"control-group\">\n      <label class=\"control-label\" for=\"textarea\">Textarea</label>\n      <div class=\"controls\">\n        <textarea class=\"input-xlarge\" id=\"textarea\" rows=\"3\"></textarea>\n      </div>\n    </div>\n      <div class=\"form-actions\">\n        <button type=\"submit\" class=\"btn btn-primary\">Save changes</button>\n        <button class=\"btn\">Cancel</button>\n      </div>\n    </fieldset>\n  </form>\n</div>";});
}});

window.require.define({"views/templates/home/options": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<div class=\"pull-left HV-rightmargin\" style=\"margin-top: 4px\">\n  <label id=\"selectAll\" class=\"checkbox\"><input type=\"checkbox\"></label>\n</div>\n<div class=\"pull-left\">\n  <a id=\"setting\" class=\"btn\" rel=\"tooltip\" data-original-title=\"Setting\">\n    <i class=\"icon-cog\"></i> Setting\n  </a>\n  <a id=\"delete\" class=\"btn\" rel=\"tooltip\" data-original-title=\"Delete\">\n    <i class=\"icon-trash\"></i> Delete\n  </a>\n</div>\n<div class=\"clearfix\"></div>\n";});
}});

window.require.define({"views/templates/home_page": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<div class=\"HV-padding HV-whitebg HV-border\">\n  <div id=\"home-header\">\n    <h1>HomePage</h1>\n  </div>\n  <div class=\"row-fluid\">\n    <div class=\"span4\">\n      <h3>Seeking job</h3>\n      <p>Some there hwerlajkdfl jl alsd l  a ldfjamhem ishan is hard at work preparing for big night . tomorrow the 11 year old syrian</p>\n      \n      <button id=\"twitterlogin\" onClick=\"window.open('/auth/twitter', 'twitter', 'toolbar,width=500,height=400,scrollbars=yes') \">Signup using twitter</button>\n\n      <button id=\"twitterlogin\" onClick=\"window.open('/auth/linkedin', 'linkedin', 'toolbar,width=500,height=400,scrollbars=yes') \">Signup using linkedin</button>\n\n\n    </div>\n    <div class=\"span4\">\n      <h3>Referring job</h3>\n      <p>Some there hwerlajkdfl jl alsd l  a ldfjamhem ishan is hard at work preparing for big night . tomorrow the 11 year old syrian</p>\n      <a href=\"#\" class=\"btn btn-primary\" href=\"#\">Login with twitter</a>\n    </div>\n    <div class=\"span4\">\n      <h3>Providing job</h3>\n      <p>Some there hwerlajkdfl jl alsd l  a ldfjamhem ishan is hard at work preparing for big night . tomorrow the 11 year old syrian</p>\n      <a href=\"#provider/12312\" class=\"btn btn-primary\" href=\"#\">Login with Twitter</a>\n    </div>\n  </div>\n</div>\n";});
}});

window.require.define({"views/templates/jobs": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<div class=\"HV-padding HV-whitebg HV-border\">\n  <div id=\"data\"></div> \n  <div class=\"clearfix\"></div>\n</div>\n";});
}});

window.require.define({"views/templates/jobs/data": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n        <li style=\"\n    float: left;\n    margin-right: 10px;\n\">\n<a href=\"#tags/";
    stack1 = depth0;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">\n            <span class=\"badge badge-warning\">";
    stack1 = depth0;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</span>\n          </a>\n          </li>\n          ";
    return buffer;}

  function program3(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n    ";
    foundHelper = helpers.written;
    stack1 = foundHelper || depth0.written;
    stack2 = helpers['if'];
    tmp1 = self.program(4, program4, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n    ";
    foundHelper = helpers.posted_in;
    stack1 = foundHelper || depth0.posted_in;
    stack2 = helpers['if'];
    tmp1 = self.program(6, program6, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n    ";
    foundHelper = helpers.location;
    stack1 = foundHelper || depth0.location;
    stack2 = helpers['if'];
    tmp1 = self.program(8, program8, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n\n    ";
    foundHelper = helpers.salary;
    stack1 = foundHelper || depth0.salary;
    stack2 = helpers['if'];
    tmp1 = self.program(10, program10, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n    ";
    return buffer;}
  function program4(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n    <li><strong>Written by : </strong>";
    foundHelper = helpers.written;
    stack1 = foundHelper || depth0.written;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "written", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</li>\n    ";
    return buffer;}

  function program6(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n    <li><strong>Posted in : </strong>";
    foundHelper = helpers.posted_in;
    stack1 = foundHelper || depth0.posted_in;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "posted_in", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</li>\n    ";
    return buffer;}

  function program8(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n    <li><strong>Location : </strong>";
    foundHelper = helpers.location;
    stack1 = foundHelper || depth0.location;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "location", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</li>\n    ";
    return buffer;}

  function program10(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n    ";
    foundHelper = helpers.salary;
    stack1 = foundHelper || depth0.salary;
    stack2 = helpers['with'];
    tmp1 = self.program(11, program11, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n    ";
    return buffer;}
  function program11(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n    <li><strong>Salary : </strong>";
    foundHelper = helpers.lowercap;
    stack1 = foundHelper || depth0.lowercap;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "lowercap", { hash: {} }); }
    buffer += escapeExpression(stack1) + " to ";
    foundHelper = helpers.uppercap;
    stack1 = foundHelper || depth0.uppercap;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "uppercap", { hash: {} }); }
    buffer += escapeExpression(stack1) + " ";
    foundHelper = helpers.units;
    stack1 = foundHelper || depth0.units;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "units", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</li>\n    ";
    return buffer;}

  function program13(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n  <div id=\"applied\" class=\"HV-jobs-border-bottom\">\n    <!--div.pull-left*6>a[href=#]>img[src=http://placehold.it/200X200]-->\n    <div class=\"pull-left\"><span class=\"label label-info\">Applied</span></div>\n    ";
    foundHelper = helpers.applied;
    stack1 = foundHelper || depth0.applied;
    stack2 = helpers.each;
    tmp1 = self.program(14, program14, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n    <div class=\"clearfix\"></div>\n  </div>\n  ";
    return buffer;}
  function program14(depth0,data) {
    
    var buffer = "", stack1;
    buffer += " \n    <div class=\"pull-left HV-jobs-border-bottom\">\n      <a href=\"";
    stack1 = depth0.resources;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.resume);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this.resources.resume", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">\n        <img src=\"";
    stack1 = depth0.resources;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.image);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this.resources.image", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" alt=\"";
    stack1 = depth0.name;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this.name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">\n      </a>\n    </div>\n    ";
    return buffer;}

  function program16(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n    <div id=\"description\" class=\"HV-jobs-padding-top\">\n      <p>";
    foundHelper = helpers.description;
    stack1 = foundHelper || depth0.description;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "description", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</p>\n  ";
    return buffer;}

    buffer += "<div class=\"page-header HV-jobs-border-bottom\"><h1>";
    foundHelper = helpers.name;
    stack1 = foundHelper || depth0.name;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</h1></div>\n<div id=\"tags\">\n<ul class=\"unstyled\">\n  ";
    foundHelper = helpers.tags;
    stack1 = foundHelper || depth0.tags;
    stack2 = helpers.each;
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n          <li class=\"clearfix\"></li>\n      </ul>\n  </div><div class=\"meta HV-jobs-border-bottom\">\n  <ul class=\"unstyled\">\n    ";
    foundHelper = helpers.meta;
    stack1 = foundHelper || depth0.meta;
    stack2 = helpers['with'];
    tmp1 = self.program(3, program3, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n    </ul>\n  </div>\n  ";
    foundHelper = helpers.applied;
    stack1 = foundHelper || depth0.applied;
    stack2 = helpers['if'];
    tmp1 = self.program(13, program13, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n  ";
    foundHelper = helpers.description;
    stack1 = foundHelper || depth0.description;
    stack2 = helpers['if'];
    tmp1 = self.program(16, program16, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n";
    return buffer;});
}});

window.require.define({"views/templates/jobs/header": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<h1>Quantitative Trading Associate - IIT (2-5 yrs)</h1>\n";});
}});

window.require.define({"views/templates/jobs1": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", foundHelper, self=this;


    return buffer;});
}});

window.require.define({"views/templates/resume": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<!--Yahan par headerView ka content render hoga-->\n<div class=\"pull-left\">\n	<!-- Yahan par resume ka container hoga -->\n	<div id=\"resumeContainer\">\n		<!-- Yahan par saare pages ka container hoga hogein -->\n\n	</div>\n	<div class=\"clearfix\"></div>\n</div>\n";});
}});

window.require.define({"views/templates/resume/annotations": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n";
    stack1 = depth0;
    stack2 = helpers['with'];
    tmp1 = self.program(2, program2, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n";
    return buffer;}
  function program2(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n<div class=\"annotationWrapper\">\n	<!-- 1st annotation -->\n  <div class=\"annotation\" style=\"top:";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.top);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.top", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px; left: ";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.left);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.left", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px; width:";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.width);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.width", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px\">\n		<div id=\"anno\">\n			<div class=\"HV-anno-yellow\">\n				<a href=\"#\">\n          <div style=\"height:";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.height);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.height", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px\"></div>\n				</a>\n			</div>\n		</div>\n	</div>\n</div>\n";
    return buffer;}

    buffer += "<!--Each annotation will have its own wrapper-->\n";
    foundHelper = helpers.annotations;
    stack1 = foundHelper || depth0.annotations;
    stack2 = helpers.each;
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n";
    return buffer;});
}});

window.require.define({"views/templates/resume/detail": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n";
    stack1 = depth0;
    stack2 = helpers['with'];
    tmp1 = self.program(2, program2, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n";
    return buffer;}
  function program2(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n<div class=\"annotationWrapper HV-overlay\">\n\n	<!-- 1st annotation -->\n  <div class=\"annotation HV-anno-active\" style=\"top:";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.top);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.top", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px; left: ";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.left);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.left", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px; width:";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.width);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.width", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px\">\n\n		<!-- Header -->\n		<div id=\"annotationheader\" class=\"HV-res-margin-bottom\">\n      <h4>";
    foundHelper = helpers.title;
    stack1 = foundHelper || depth0.title;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</h4>\n		</div>\n		<!-- Image box -->\n    <div id=\"anno\" class=\"HV-anno-shadow-box HV-res-margin-bottom\" style=\"background : url('/images/700x/resume_1.png'); background-position : -";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.left);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.left", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px -";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.top);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.top", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px;\">\n			<div class=\"HV-anno-yellow\">\n				<a href=\"#\">\n          <div style=\"height:";
    foundHelper = helpers.image;
    stack1 = foundHelper || depth0.image;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.height);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "image.height", { hash: {} }); }
    buffer += escapeExpression(stack1) + "px\"></div>\n				</a>\n			</div>\n		</div>\n		<!-- Audio -->\n		<div id=\"annotationaudio\">\n			<div class=\"pull-left\">play icon</div>\n			<div class=\"pull-right\">\n				<p>3:42</p>\n			</div>\n			<div class=\"clearfix\"></div>\n		</div>\n		<!-- Description -->\n		<div id=\"annotationdesc\">";
    foundHelper = helpers.notes;
    stack1 = foundHelper || depth0.notes;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "notes", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</div>\n	</div>\n</div>\n";
    return buffer;}

    foundHelper = helpers.annotations;
    stack1 = foundHelper || depth0.annotations;
    stack2 = helpers.each;
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n";
    return buffer;});
}});

window.require.define({"views/templates/resume/main": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<div id=\"pages\" class=\"HV-res-margin-bottom HV-res-border-bottom\">\n  <!-- Yahan par ek ke baad ek pages hogein -->\n  <div id=\"page\" class=\"HV-pos-relative\">\n    <img src=\"/images/700x/resume_1.png\" alt=\"\">\n    <!-- Yahan par agar koi annotations hogein to yahan is container mein hogein -->\n    <div id=\"annotations\">\n      <!-- 1st annotation -->\n      <div class=\"annotation\" style=\"top:130px; left: 55px; width:600px\">\n        <div class=\"HV-overlay\">\n          <div id=\"annoWhere\" style=\"background : url('/images/700x/resume_1.png'); background-position : -47px -345px;\">\n            <div class=\"HV-anno-yellow\">\n              <a href=\"#\">\n                <div style=\"height:60px\"></div>\n              </a>\n            </div>\n          </div>\n        </div>\n      </div>\n      <!-- 1st annotation -->\n      <div class=\"HV-overlay\">\n        <div class=\"annotation HV-anno-active\" style=\"top:343px; left: 55px; width:600px\">\n          <div id=\"annotationheader\" class=\"HV-res-margin-bottom\">\n            <h4>Highlight for Testing</h4>\n          </div>\n          <div id=\"anno\" class=\"HV-anno-shadow-box HV-res-margin-bottom\" style=\"background : url('/images/700x/resume_1.png'); background-position : -47px -345px;\">\n            <div class=\"HV-anno-yellow\">\n              <a href=\"#\">\n                <div style=\"height:74px\"></div>\n              </a>\n            </div>\n          </div>\n          <div id=\"annotationaudio\">\n            <div class=\"pull-left\">play icon</div>\n            <div class=\"pull-right\">\n              <p>3:42</p>\n            </div>\n            <div class=\"clearfix\"></div>\n          </div>\n          <div id=\"annotationdesc\">\n            <ul>\n              <li>Morbi mattis erat neque, vel fringilla velit.</li>\n              <li>\n                Fusce commodo blandit nibh, condimentum pulvinar felis pharetra non.\n              </li>\n              <li>\n                Pellentesque sollicitudin commodo erat, eget porttitor mi eleifend vitae.\n              </li>\n              <li>Fusce a nisi sed quam luctus blandit molestie id urna.</li>\n              <li>Aliquam vulputate velit ut augue ullamcorper pulvinar.</li>\n            </ul>\n          </div>\n        </div>\n      </div>\n      <!-- highlighted annotation ends --> </div>\n    <div id=\"page\" style=\"border-top: 1px solid lightgrey\">\n      <img src=\"/images/700x/resume_2.png\" alt=\"\"></div>\n  </div>\n</div>";});
}});

window.require.define({"views/templates/resume/main1": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<div id=\"pages\" class=\"HV-res-margin-bottom HV-res-border-bottom HV-div-scrollable HV-res-border pull-left\">\n  <!--page-->\n</div>\n<div id=\"rightdata\" class=\"HV-res-padding pull-left\" style=\"width:200px;\"></div>\n<div class=\"clearfix\"></div>\n  \n";});
}});

window.require.define({"views/templates/resume/page": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, stack3, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0;

  function program1(depth0,data) {
    
    
    return "\n    <div id=\"annotations\">\n    </div>\n    ";}

    buffer += "<div id=\"pagebig\" class=\"HV-pos-relative\">\n  <img src='";
    foundHelper = helpers.resources;
    stack1 = foundHelper || depth0.resources;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.resume);
    stack2 = {};
    stack3 = "pagenumber";
    stack2['what'] = stack3;
    stack3 = "pagenumber";
    stack2['with'] = stack3;
    stack3 = "true";
    stack2['inContext'] = stack3;
    foundHelper = helpers.replace;
    stack3 = foundHelper || depth0.replace;
    tmp1 = {};
    tmp1.hash = stack2;
    if(typeof stack3 === functionType) { stack1 = stack3.call(depth0, stack1, tmp1); }
    else if(stack3=== undef) { stack1 = helperMissing.call(depth0, "replace", stack1, tmp1); }
    else { stack1 = stack3; }
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "' alt=\"\" />\n    <!--annotations -->\n    ";
    foundHelper = helpers.annotations;
    stack1 = foundHelper || depth0.annotations;
    stack2 = helpers['if'];
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n    <!--yahan par annotions div khatam hoti hai -->\n</div>\n";
    return buffer;});
}});

window.require.define({"views/templates/resume/pages": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


    buffer += "<div id=\"pagesbig\" style=\"height : ";
    foundHelper = helpers.pageheight;
    stack1 = foundHelper || depth0.pageheight;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "pageheight", { hash: {} }); }
    buffer += escapeExpression(stack1) + "p4\"></div>\n";
    return buffer;});
}});

window.require.define({"views/templates/resume/right": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n  \n  <ul class=\"unstyled HV-res-border-bottom HV-padding-bottom\">\n    ";
    foundHelper = helpers.tags;
    stack1 = foundHelper || depth0.tags;
    stack2 = helpers.each;
    tmp1 = self.program(2, program2, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n			<li class=\"clearfix\"></li>\n    </ul>\n    ";
    return buffer;}
  function program2(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n      <li class=\"pull-left\" style=\"margin-right:10px\"><span class=\"badge badge-warning\">";
    stack1 = depth0;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</span></li>\n    ";
    return buffer;}

  function program4(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n      <p class=\"HV-res-border-bottom HV-padding-bottom\">";
    foundHelper = helpers.description;
    stack1 = foundHelper || depth0.description;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "description", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</p>\n    ";
    return buffer;}

  function program6(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n		<div id=\"notes\">\n			<div class=\"notes\">\n				<h4>Notes</h4>\n        \n        <ol>\n          ";
    foundHelper = helpers.annotations;
    stack1 = foundHelper || depth0.annotations;
    stack2 = helpers.each;
    tmp1 = self.program(7, program7, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n        </ol>\n        \n			</div>\n	  <div class=\"clearfix\"></div>\n  </div>\n  ";
    return buffer;}
  function program7(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n          <li><a href=\"#\"><p>";
    stack1 = depth0.title;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this.title", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</p></a></li>\n          ";
    return buffer;}

    buffer += "  <h3>";
    foundHelper = helpers.name;
    stack1 = foundHelper || depth0.name;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</h3>\n  ";
    foundHelper = helpers.tags;
    stack1 = foundHelper || depth0.tags;
    stack2 = helpers['if'];
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n    ";
    foundHelper = helpers.description;
    stack1 = foundHelper || depth0.description;
    stack2 = helpers['if'];
    tmp1 = self.program(4, program4, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n\n    ";
    foundHelper = helpers.annotations;
    stack1 = foundHelper || depth0.annotations;
    stack2 = helpers['if'];
    tmp1 = self.program(6, program6, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n<div class=\"clearfix\"></div>\n";
    return buffer;});
}});

