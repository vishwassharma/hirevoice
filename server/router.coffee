sysPath = require 'path'
path = './public'
models = require './models'

index = (req, res) ->
  console.log "[App] Rendering '/' uri"
  context =
    title : 'My Title'

  res.render 'index.html', context

login = (req, res) ->
  console.log "[App] Rendering '/login' uri"
  #res.sendfile sysPath.join path, 'login.html'
  res.render 'login.html'

jobs = (req, res, next) ->
  console.log "[App] Fetching resuls from jobs database"
  query = models.JobModel.find {}
  query.exec (err, docs) ->
    res.json docs

seekers = (req, res, next) ->
  console.log "[App] Fetching resuls from jobs database"
  query = models.SeekerModel.find {}
  query.exec (err, docs) ->
    res.json docs

exports.index = index
exports.login = login
exports.jobs = jobs
exports.seekers = seekers
