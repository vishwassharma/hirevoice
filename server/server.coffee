express = require 'express'
gzippo = require 'gzippo'
sysPath = require 'path'
everyauth = require 'everyauth'
settings = require './setting'

# Twitter
everyauth.twitter
  .consumerKey(settings.twitter.appId)
  .consumerSecret(settings.twitter.secret)
  .findOrCreateUser( (session, accessToke, accessTokenSecret, twitUserData) ->
    promise = new @Promise
    promise.fulfill twitUserData
    promise
  ).redirectPath '/'

everyauth.linkedin
  .consumerKey(settings.linkedin.appId)
  .consumerSecret(settings.linkedin.secret)
  .findOrCreateUser((sessions, token, secretToken, linkedinData) ->
    promise = new @Promise
    promise.fulfill linkedinData
    promise
  ).redirectPath '/'

# make a mongoose connection
mongoose = require 'mongoose'
mongoose.connect settings.db
everyauth = require 'everyauth'

# get the global variables here
router = require './router'

MemStore = express.session.MemoryStore

everyauth.debug = true

exports.startServer = (port, path, callback = (->)) ->
  # Starting the server here
  server = express.createServer()

  # declare middle ware here
  server.configure ->
    #server.use express.logger()
    server.use express.bodyParser()
    #server.use express.methodOverride()
    server.use express.cookieParser()
    server.use express.session
      secret : 'adofi02394lk324j2lkjlsdfjlsdj09329jklsdajlfd'
      store : new MemStore
        reapInterval : 50000 * 10
    #express.favicon()
    server.use express.csrf()
    server.use everyauth.middleware()
    #server.use router
    server.use express.static path
    #everyauth.helpExpress(server)
    server.use (req, response, next) ->
      response.header 'Cache-Control', 'no-cache'
      next()
    server.use express.errorHandler
          dumpExceptions: true
          showStack: true
  server.get '/', router.index
  #server.get '/login', router.login
  server.get '/api/jobs', router.jobs
  server.get '/api/seekers', router.seekers

  server.all "/*", (request, response) ->
    response.sendfile sysPath.join path, '404.html'

  # everyauth helper
  everyauth.helpExpress(server)

  server.listen parseInt port, 10
  server.on 'listening', callback
  server
